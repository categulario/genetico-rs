/// Un individuo es un vector de 150 entradas

/// Las estructuras que implementan este trait son compatibles como individuos
/// de este algoritmo genético.
pub trait Individuo {
    fn aptitud(&self) -> f64;
}

/// Un individuo chafa
pub struct IndividuoReal {
    data: Vec<bool>,
}

impl IndividuoReal {
    pub fn new() -> IndividuoReal {
        IndividuoReal {
            data: Vec::new(),
        }
    }
}

impl Individuo for IndividuoReal {
    fn aptitud(&self) -> f64 {
        1.0
    }
}

