use rand::{Rng, SeedableRng};

mod individuo;

use individuo::IndividuoReal;

type Poblacion = Vec<IndividuoReal>;

fn crear_poblacion(generador: impl Rng) -> Poblacion {
    Vec::new()
}

fn seleccionar(poblacion: &Poblacion) -> Poblacion {
    Vec::new()
}

fn cruzar(poblacion: Poblacion, probabilidad: f64) -> Poblacion {
    Vec::new()
}

fn mutar(poblacion: Poblacion, probabilidad: f64) -> Poblacion {
    Vec::new()
}

fn combinar(poblacion_original: Poblacion, mutados: Poblacion) -> Poblacion {
    Vec::new()
}

const MAX_GENERACIONES: u32 = 2500;

fn main() {
    // El generador de números aleatorios se inicializa aquí,
    let mut rng = rand_pcg::Pcg32::seed_from_u64(123);

    // TODO crear una población de N individuos
    let mut poblacion = crear_poblacion(rng);
    let mut num_generacion = 0;

    loop {
        // TODO Seleccionar K individuos por ruleta
        let seleccionados = seleccionar(&poblacion);

        // TODO Cruza
        let p_cruza = 0.5; // probabilidad de cruzarse
        let cruzados = cruzar(seleccionados, p_cruza);

        // TODO Mutación
        let p_mutacion = 0.5;
        let mutados = mutar(cruzados, p_mutacion);

        // TODO Recombinación
        poblacion = combinar(poblacion, mutados);

        num_generacion += 1;

        if num_generacion >= MAX_GENERACIONES {
            break;
        }
    }
}
